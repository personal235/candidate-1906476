This is the test automation project to check if applicants require a visa to visit the UK.


### Automation framework with Page Object Model design using Selenium

IntelliJ
Maven
Java
GitLab/Git

### How to run the test through Maven
Within the command line set the path to point to the root of the project the run Maven with required properties, e.g.

mvn test


API Testing: Postman collection
### Location of collection

Path for the collection: src/test/java/postman collection
