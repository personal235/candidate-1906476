package uk.gov.steps;

import io.cucumber.java.en.And;
import uk.gov.page.TourismPage;

public class TourismSteps {

    TourismPage tourismPage = new TourismPage();

    @And("I state I am not travelling with or visiting a partner or family")
    public void iStateIAmNotTravellingWithOrVisitingAPartnerOrFamily () {
        tourismPage.clickNo_RadioBtn();
    }
}