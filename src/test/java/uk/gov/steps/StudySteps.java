package uk.gov.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import uk.gov.page.CheckUkVisaPage;
import uk.gov.page.StudyPage;

public class StudySteps {

    StudyPage studyPage = new StudyPage();
    CheckUkVisaPage checkUkVisaPage = new CheckUkVisaPage();

    @And("I state I am intending to stay for more than six months")
    public void iStateIAmIntendingToStayForMoreThanSixMonths () {
        studyPage.selectStudyDuration();
    }

    @When("I submit form")
    public void iSubmitForm () {
        checkUkVisaPage.clickNextStep();
    }

    @Then("I will be informed {string}")
    public void iWillBeInformedYouLlNeedAVisaToStudyInTheUK ( String expected ) {
        studyPage.verifyPageTitle(expected);
    }
}