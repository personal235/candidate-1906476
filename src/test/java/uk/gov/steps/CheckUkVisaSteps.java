package uk.gov.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import uk.gov.domain.CheckUkVisaDomain;
import uk.gov.page.HomePage;

import java.io.IOException;

public class CheckUkVisaSteps {

    HomePage homePage = new HomePage();
    CheckUkVisaDomain checkUkVisaDomain = new CheckUkVisaDomain();

    @Given("I am on UK Visa home page")
    public void iAmOnUKVisaHomePage () throws IOException {
        homePage.visit();
    }

    @And("I select applicants nationality {string} and the reason {string}")
    public void iSelectApplicantsNationalityAndTheReasonStudy ( String country, String reason ) {
        checkUkVisaDomain.confirmStudyVisaIsNeeded(country, reason);
    }

    @And("I select applicants nationality {string} and reason as {string}")
    public void iSelectApplicantsNationalityJapanAndReasonAsTourism ( String country, String reason ) {
        checkUkVisaDomain.confirmVisaIsNeeded(country, reason);
    }

    @And("I select applicants nationality {string} and travelling reason {string}")
    public void iSelectApplicantsNationalityRussiaAndTravellingReasonTourism ( String country, String reason ) {
        checkUkVisaDomain.confirmVisitVisaIsNeeded(country, reason);
    }
}