Feature: Visa Check

  Background:
    Given I am on UK Visa home page

  Scenario: Applicant requires UK study visa
    And I select applicants nationality 'Japan' and the reason 'Study'
    And I state I am intending to stay for more than six months
    When I submit form
    Then I will be informed 'You’ll need a visa to study in the UK'

  Scenario: Applicant does not require UK study visa
    And I select applicants nationality 'Japan' and reason as 'Tourism'
    When I submit form
    Then I will be informed 'You do not need a visa if you’re staying up to 6 months'

  Scenario: Applicant requires UK tourism visa
    And I select applicants nationality 'Russia' and travelling reason 'Tourism'
    And I state I am not travelling with or visiting a partner or family
    When I submit form
    Then I will be informed 'You’ll need a visa to come to the UK'




