package uk.gov.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "uk.gov.steps",
        features = ("src/test/java/uk/gov/feature"),
        plugin = {"json:target/cucumber.json", "html:target/site/cucumber-pretty"}
)

public class AllTests {

}