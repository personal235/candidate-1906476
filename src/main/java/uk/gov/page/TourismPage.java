package uk.gov.page;

import org.openqa.selenium.By;

import static uk.gov.base.DriverContext.driver;

public class TourismPage {

    private By no_RadioButton = By.id("response-1");

    public TourismPage clickNo_RadioBtn () {
        driver.findElement(no_RadioButton).click();
        return this;
    }
}