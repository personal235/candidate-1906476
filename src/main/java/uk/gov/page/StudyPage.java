package uk.gov.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertEquals;
import static uk.gov.base.DriverContext.driver;

public class StudyPage {

    private By longerThanSixMonthsRadioButton = By.id("response-1");
    private static By pageTitleText = By.xpath("//*[@id=\"result-info\"]/div[2]/h2");

    public StudyPage clickLongerThanSixMonths () {
        driver.findElement(longerThanSixMonthsRadioButton).click();
        return this;
    }

    public String getPageTitleText () {
        WebElement pageTitle = driver.findElement(pageTitleText);
        System.out.println();
        return pageTitle.getText();
    }

    public StudyPage selectStudyDuration () {
        clickLongerThanSixMonths();
        return this;
    }

    public void verifyPageTitle ( String expected ) {
        String actual = getPageTitleText();
        assertEquals(expected, actual);
        System.out.println(getPageTitleText());
    }
}