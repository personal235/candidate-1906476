package uk.gov.page;

import uk.gov.base.Base;
import uk.gov.config.Settings;

import java.io.IOException;

import static uk.gov.base.Browser.startBrowserSession;
import static uk.gov.base.DriverContext.driver;

public class HomePage extends Base {

    public HomePage visit () throws IOException {
        startBrowserSession();
        driver.manage().window().maximize();
        driver.get(Settings.siteUrl);
        return this;
    }
}