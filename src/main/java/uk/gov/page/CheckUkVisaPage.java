package uk.gov.page;

import org.openqa.selenium.By;
import uk.gov.base.Base;

import static uk.gov.base.DriverContext.driver;

public class CheckUkVisaPage extends Base {

    private By startNowButton = By.xpath("//*[@id=\"get-started\"]/a");
    private By nationalityField = By.id("response");
    private By nextStepButton = By.xpath("//*[@id=\"current-question\"]/button");
    private By studyRadioButton = By.id("response-2");
    private By tourismRadioButton = By.id("response-0");


    public CheckUkVisaPage clickStartNow () {
        driver.findElement(startNowButton).click();
        return this;
    }

    public CheckUkVisaPage clickNextStep () {
        driver.findElement(nextStepButton).click();
        return this;
    }

    public CheckUkVisaPage enterNationality ( String country ) {
        driver.findElement(nationalityField).sendKeys(country);
        return this;
    }

    public CheckUkVisaPage clickStudy ( String reason ) {
        driver.findElement(studyRadioButton).click();
        return this;
    }

    public CheckUkVisaPage clickTourism ( String reason ) {
        driver.findElement(tourismRadioButton).click();
        return this;
    }
}