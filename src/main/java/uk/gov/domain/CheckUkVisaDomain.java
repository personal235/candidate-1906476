package uk.gov.domain;

import uk.gov.page.CheckUkVisaPage;

public class CheckUkVisaDomain {

    CheckUkVisaPage checkUkVisaPage = new CheckUkVisaPage();

    public CheckUkVisaDomain confirmStudyVisaIsNeeded ( String country, String reason ) {
        checkUkVisaPage.clickStartNow()
                .enterNationality(country)
                .clickNextStep()
                .clickStudy(reason)
                .clickNextStep();
        return this;
    }

    public CheckUkVisaDomain confirmVisaIsNeeded ( String country, String reason ) {
        checkUkVisaPage.clickStartNow()
                .enterNationality(country)
                .clickNextStep()
                .clickTourism(reason);
        return this;
    }

    public CheckUkVisaDomain confirmVisitVisaIsNeeded ( String country, String reason ) {
        checkUkVisaPage.clickStartNow()
                .enterNationality(country)
                .clickNextStep()
                .clickTourism(reason)
                .clickNextStep();
        return this;
    }
}